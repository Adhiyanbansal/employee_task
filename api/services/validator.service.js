const { validationResult, check, query } = require("express-validator");

exports.validator = (type) => {
  switch (type) {
    case "create": {
      return [
        check("name").notEmpty().withMessage("Name is required!"),

        check("email")
          .notEmpty()
          .withMessage("Email is required")
          .isEmail()
          .withMessage("Email should be in format"),
      ];
    }
  }
};
