const jwt = require('jsonwebtoken');

const secret = process.env.NODE_ENV === 'production' ? process.env.JWT_SECRET : 'secret';

const authService = () => {
  const issue = (payload) => jwt.sign(payload, secret, { expiresIn: 10800 });
  const onetime_issue = (payload) => jwt.sign(payload, secret, { expiresIn: 7200 }); //2 hours
  const verify = (token, cb) => jwt.verify(token, secret, {}, cb);

  return {
    issue,
    onetime_issue,
    verify,
  };
};

module.exports = authService;
