const Employee = require("../models/Employee");

const EmployeeController = () => {
  const createEmployee = async (req, res) => {
    const { body } = req;

    try {
      const emailCheck = await Employee.findOne({
        where: {
          email: body.email,
        },
      });
      if (!emailCheck) {
        return res.status(400).json({
          msg: "Bad Request: Email already Exist",
        });
      }
      const employee = await Employee.create({
        name: body.name,
        email: body.email,
        phone_number: body.phone_number,
        country_code: body.country_code,
        dob: body.dob,
        department: body.department,
        salary: body.salary,
        experience: body.experience,
        designation: body.designation,
      });

      if (!employee) {
        return res.status(400).json({
          msg: "Bad Request: Model not found",
        });
      }

      return res.status(200).json({
        employee,
      });
    } catch (err) {
      console.error(err);
      return res.status(500).json({
        msg: "Internal server error",
      });
    }
  };

  const listEmployee = async (req, res) => {
    try {
      const employees = await Employee.findAll();
      const count = await Employee.count();
      return res.status(200).json({
        success: true,
        data: { employees, count },
      });
    } catch (err) {
      console.log(err);
      return res.status(500).json({
        msg: "Internal server error",
      });
    }
  };

  const register = async (req, res) => {
    const { body } = req;

    if (body.password === body.confirmPassword) {
      try {
        const user = await Employee.create({
          email: body.email,
          password: body.password,
          name: body.name,
          phone_number: body.phone_number,
          country_code: body.country_code,
          dob: body.dob,
          department: body.department,
          salary: body.salary,
          experience: body.experience,
          designation: body.designation,
        });
        const token = authService().issue({ id: user.id });

        return res.status(200).json({ token, user });
      } catch (err) {
        console.log(err);
        return res.status(500).json({ msg: "Internal server error" });
      }
    }

    return res.status(400).json({ msg: "Bad Request: Passwords don't match" });
  };

  const login = async (req, res) => {
    const { email, password } = req.body;

    if (email && password) {
      try {
        const employee = await Employee.findOne({
          where: {
            email,
          },
        });

        if (!employee) {
          return res.status(400).json({ msg: "Bad Request: User not found" });
        }

        if (bcryptService().comparePassword(password, employee.password)) {
          const token = authService().issue({
            id: employee.id,
            name: employee.name,
          });

          return res.status(200).json({ token, employee });
        }

        return res.status(401).json({ msg: "Unauthorized" });
      } catch (err) {
        console.log(err);
        return res.status(500).json({ msg: "Internal server error" });
      }
    }

    return res
      .status(400)
      .json({ msg: "Bad Request: Email or password is wrong" });
  };

  return {
    createEmployee,
    listEmployee,
    register,
    login,
  };
};

module.exports = EmployeeController;
