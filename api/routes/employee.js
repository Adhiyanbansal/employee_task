const router = require("express").Router();
const EmployeeContoller = require("../controllers/EmployeeController");

const auth = require("../policies/auth.policy");
const { validator } = require("../services/validator.service");
router.use("*", (req, res, next) => auth(req, res, next));

//Create Employee
router.post("/", EmployeeContoller().createEmployee);

//List Employee
router.get("/", EmployeeContoller().listEmployee);

//register Employee
router.post("/register", EmployeeContoller().register);

//login Employee
router.post("/authorize", EmployeeContoller().login);

module.exports = router;
