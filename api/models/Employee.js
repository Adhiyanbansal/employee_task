const Sequelize = require('sequelize');

const sequelize = require('../../config/database');

const tableName = 'employees';

const Employee = sequelize.define('Employee', {
    name: {
        type: Sequelize.STRING,
    },
    email: {
        type: Sequelize.STRING,
    },
    password: {
        type: Sequelize.STRING,
    },
    phone_number: {
        type: Sequelize.STRING,
    },
    country_code: {
        type: Sequelize.STRING,
    },
    dob: {
        type: Sequelize.STRING,
    },
    department: {
        type: Sequelize.STRING,
    },
    salary: {
        type: Sequelize.STRING,
    },
    experience: {
        type: Sequelize.STRING,
    },
    designation: {
        type: Sequelize.STRING,
    },
}, {
    tableName
});


module.exports = Employee;
