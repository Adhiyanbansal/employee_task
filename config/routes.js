/**
 * Import Route files and set in express middleware
 */

exports.set_routes = (app) => {
    const employees = require('../api/routes/employee');
   ;

    app.use('/api/employee', employees);
  
}